const {ipcMain} = require('electron')

ipcMain.on('synchronous-message', (event, arg) => {
  console.log(arg,'*********')  // prints "ping"
  var d = [
    {usuario_id:1, edad:10, calle: 'LOS CUARTOS II', nombre:'JOSE'},
    {usuario_id:2, edad:10, calle: 'LOS CUARTOS II', nombre:'CARLOS'},
    {usuario_id:3, edad:10, calle: 'LOS CUARTOS II', nombre:'ARMANDO'},
    {usuario_id:4, edad:10, calle: 'LOS CUARTOS II', nombre:'LETY'},
    {usuario_id:5, edad:10, calle: 'LOS CUARTOS II', nombre:'OSVALDO'}
  ]
  event.sender.send('asynchronous-reply', d)
})
