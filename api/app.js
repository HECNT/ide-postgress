var express = require('express')
var api = express()

// Add headers
api.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

api.get('/', init)

function init(req, res) {
  var d = [
    {usuario_id:1, edad:10, calle: 'LOS CUARTOS II', nombre:'JOSE'},
    {usuario_id:2, edad:10, calle: 'LOS CUARTOS II', nombre:'CARLOS'},
    {usuario_id:3, edad:10, calle: 'LOS CUARTOS II', nombre:'ARMANDO'},
    {usuario_id:4, edad:10, calle: 'LOS CUARTOS II', nombre:'LETY'},
    {usuario_id:5, edad:10, calle: 'LOS CUARTOS II', nombre:'OSVALDO'}
  ]

  res.json(d)
}

api.listen(4001, function(){
  console.log('Escuchando en el puerto 4001');
})
