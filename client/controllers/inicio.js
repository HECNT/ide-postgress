var app = angular.module('inicio', []);
var path = require('path')
var url = 'http://localhost:4001'
const {ipcRenderer} = require('electron')
var file = path.resolve(__dirname, 'test.db')

app.controller('inicioCtrl', function ($scope, $http){
  $scope.btnDerecho  = btnDerecho
  $scope.btnGetLocal = btnGetLocal
  $scope.btnGetApi   = btnGetApi
  $scope.btnSql      = btnSql
  $scope.btnConexion = btnConexion;

  function btnDerecho() {
    $('#right').sidebar('toggle');
  }

  function btnConexion() {
    $('.ui.modal').modal('show');
  }

  function btnGetLocal() {
    ipcRenderer.send('synchronous-message', 'ping')
    ipcRenderer.on('asynchronous-reply', (event, arg) => {
      console.log(arg,'LOCAL') // prints "pong"
      $scope.msg = 'LOCAL'
      $scope.list = arg
    })
  }

  function btnGetApi() {
    $http.get(url + '/')
    .then(function(res){
      console.log(res.data ,'API');
      $scope.list = res.data
      $scope.msg = 'API'
    })
  }

  function btnSql() {

  }

})
